class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        res = begin = 0
        sets = set()
        for end in range(len(s)):
            while (s[end] in sets):
                sets.remove(s[begin])
                begin += 1

            sets.add(s[end])
            res = max(res, end - begin + 1)

        return res

sol = Solution()
print(sol.lengthOfLongestSubstring("pwwkew"))