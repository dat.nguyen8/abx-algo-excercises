from typing import List

class Solution:
    def selfDividingNumbers(self, left: int, right: int) -> List[int]:
        self_dividings = list()
        
        for number in range(left, right + 1):
            self_dividing = True
            number_copy = number;

            while number_copy:
                if number_copy % 10 == 0 or number % (number_copy % 10):
                    self_dividing = False
                    break
                number_copy //= 10
            
            if self_dividing:
                self_dividings.append(number);
        
        return self_dividings;

sol = Solution()
print(sol.selfDividingNumbers(1, 22))