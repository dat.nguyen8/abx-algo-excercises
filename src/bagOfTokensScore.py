from typing import List

class Solution:
    def bagOfTokensScore(self, tokens: List[int], P: int) -> int:
        tokens.sort()

        res = count = 0
        left = 0;
        right = len(tokens) - 1
        while left <= right:
            if tokens[left] <= P:
                P -= tokens[left]
                left += 1
                count += 1
                res = max(res, count)
            elif res:
                P += tokens[right]
                right -= 1
                count -= 1
            else:
                break

        return res


sol = Solution()
print(sol.bagOfTokensScore([100, 200, 300, 400], 200))
print(sol.bagOfTokensScore([100, 200], 150))