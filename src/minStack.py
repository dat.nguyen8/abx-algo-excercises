from queue import LifoQueue

class MinStack:

    def __init__(self):
        """
        initialize your data structure here.
        """
        self.stack = LifoQueue(1000000)

    def push(self, x: int) -> None:
        if self.stack.empty():
            self.stack.put([x, x])
        else:
            ele = self.get()
            self.stack.put([x, min(x, ele[1])])

    def pop(self) -> None:
        self.stack.get()

    def top(self) -> int:
        return self.get()[0]

    def getMin(self) -> int:
        return self.get()[1]
    
    def get(self):
        ele = self.stack.get()
        self.stack.put(ele)
        return ele

# Your MinStack object will be instantiated and called as such:
obj = MinStack()
obj.push(4)
obj.push(6)
obj.push(5)
obj.pop()
obj.pop()
param_3 = obj.top()
param_4 = obj.getMin()

print(param_3)
print(param_4)
