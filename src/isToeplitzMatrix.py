from typing import List

class Solution:
    def isToeplitzMatrix(self, matrix: List[List[int]]) -> bool:
        for i in range(0, len(matrix)):
            i_copy = i + 1; j = 1
            while i_copy < len(matrix) and j < len(matrix[0]):
                if matrix[i_copy][j] != matrix[i_copy - 1][j - 1]:
                    return False
                i_copy += 1; j += 1
        
        for j in range(1, len(matrix[0])):
            i = 1; j_copy = j + 1
            while i < len(matrix) and j_copy < len(matrix[0]):
                if matrix[i][j_copy] != matrix[i - 1][j_copy - 1]:
                    return False
                i += 1; j_copy += 1
        
        return True

sol = Solution()
print(sol.isToeplitzMatrix([
  [1, 2, 3, 4],
  [5, 1, 2, 3],
  [9, 5, 1, 2]
]))