# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    def traverseTrees(self, t1: TreeNode, t2: TreeNode) -> TreeNode:
        if t1 is None and t2 is None:
            return None
        
        t = TreeNode(0)
        t.val += t1.val if not t1 is None else 0
        t.val += t2.val if not t2 is None else 0
        
        t.left = self.traverseTrees(t1.left if not t1 is None else None, t2.left if not t2 is None else None)
        t.right = self.traverseTrees(t1.right if not t1 is None else None, t2.right if not t2 is None else None)
        return t

    def mergeTrees(self, t1: TreeNode, t2: TreeNode) -> TreeNode:
        return self.traverseTrees(t1, t2)