import math

class Solution:
    def findComplement(self, num: int) -> int:
        return num ^ (-1 + (1 << int(math.log(num, 2)) + 1))

sol = Solution()
print(sol.findComplement(5))